<?php
/**
 * Created by PhpStorm.
 * User: MF
 * Date: 2018/6/24
 * Time: 22:34
 */

// 启动文件

require(__DIR__ . '/BaseYii.php');

class Yii extends \yii\BaseYii
{
}

// 注册器
// 等于 Yii::autoload
spl_autoload_register(['Yii', 'autoload'], true, true);
