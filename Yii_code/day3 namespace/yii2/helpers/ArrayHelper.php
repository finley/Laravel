<?php
/**
 * Created by PhpStorm.
 * User: MF
 * Date: 2018/7/1
 * Time: 11:33
 */

namespace yii\helpers;


class ArrayHelper
{

	public static function remove(&$array, $key, $default = null)
	{
		if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array))) {
			$value = $array[$key];
			unset($array[$key]);

			return $value;
		}

		return $default;
	}

}