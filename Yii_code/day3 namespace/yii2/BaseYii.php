<?php
/**
 * Created by PhpStorm.
 * User: MF
 * Date: 2018/6/24
 * Time: 22:14
 */

namespace yii;

class BaseYii
{

	public static function getVersion()
	{
		return '2.0.12';
	}

	public static function configure($object, $properties)
	{
		foreach ($properties as $name => $value) {
			$object->$name = $value;
		}

		return $object;
	}


	public static function autoload($className)
	{
		//  TODO
		$classFile = $className . '';

		include($classFile);

		if (!class_exists($className, false) && !interface_exists($className, false) && !trait_exists($className, false)) {
			throw new \Exception("Unable to find '$className' in file: $classFile. Namespace missing?");
		}
	}
}


