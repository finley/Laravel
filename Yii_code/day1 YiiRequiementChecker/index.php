<?php



/**
 * 
 */
class YiiRequirementChecker
{

	function check($requirements) 
	{
		if (is_string($requirements)) {
            $requirements = require($requirements);
        }

        if (!is_array($requirements)) {
            $this->usageError('Requirements must be an array, "' . gettype($requirements) . '" has been given!');
        }

        if (!isset($this->result) || !is_array($this->result)) {
            $this->result = [
                'summary' => [
                    'total' => 0,
                    'errors' => 0,
                    'warnings' => 0,
                ],
                'requirements' => [],
            ];
        }

        return $this;
	}

	function checkYii() 
	{
		if (file_exists(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'requirements.php')) {
			return $this->check(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'requirements.php');
		}
		return $this;
	}

	function usageError($message)
    {
        echo "Error: $message\n\n";
        exit(1);
    }

    function render()
    {
        if (!isset($this->result)) {
            $this->usageError('Nothing to render!');
        }
        $baseViewFilePath = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'views';
        $viewFileName = $baseViewFilePath . DIRECTORY_SEPARATOR . 'console' . DIRECTORY_SEPARATOR . 'index.php';
        $this->renderViewFile($viewFileName, $this->result);
    }

    function renderViewFile($_viewFile_, $_data_ = null, $_return_ = false)
    {
        // we use special variable names here to avoid conflict when extracting data
        if (is_array($_data_)) {
            extract($_data_, EXTR_PREFIX_SAME, 'data');
        } else {
            $data = $_data_;
        }
        if ($_return_) {
            ob_start();
            ob_implicit_flush(false);
            require($_viewFile_);

            return ob_get_clean();
        } else {
            require($_viewFile_);
        }
    }

}

$requirementChecker = new YiiRequirementChecker();

$requirements = [
    [
        'name' => 'PDO extension',
        'mandatory' => true,
        'condition' => extension_loaded('pdo'),
        'by' => 'All DB-related classes',
    ],
];

echo dirname(__FILE__) . DIRECTORY_SEPARATOR . 'requirements.php';
$requirementChecker->checkYii()->check($requirements)->render();
