<?php
/**
 * Created by PhpStorm.
 * User: MF
 * Date: 2018/6/24
 * Time: 22:14
 */

namespace yii;

class BaseYii
{

	/**
	 * @var Container the dependency injection (DI) container used by [[createObject()]].
	 * You may use [[Container::set()]] to set up the needed dependencies of classes and
	 * their initial property values.
	 * @see createObject()
	 * @see Container
	 */
	public static $container;


	public static function getVersion()
	{
		return '2.0.12';
	}

	public static function configure($object, $properties)
	{
		foreach ($properties as $name => $value) {
			$object->$name = $value;
		}

		return $object;
	}

	// https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader-examples.md
	public static function my_auto_load($className) {
		// 命名空间要用 'aa\\bb\\cc' 则路径为 'aa/bb/cc'
		$prefix = 'yii\\';

		// $base_dir = __DIR__ . '/src/';
		$base_dir = __DIR__ . '/';

		// 进行路径处理, 取 prefix 之后的当作路径和base_dir进行拼接
		$relative_class = substr($className, strlen($prefix));

		$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

		if (file_exists($file)) {
			include($file);
		}
	}
}


