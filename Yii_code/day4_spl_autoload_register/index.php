<?php
/**
 * Created by PhpStorm.
 * User: MF
 * Date: 2018/7/1
 * Time: 12:24
 */

require(__DIR__ . '/BaseYii.php');

use \yii\helpers\ArrayHelper;

class Yii extends \yii\BaseYii
{
}

// 实际 Yii::my_auto_load
spl_autoload_register(['Yii', 'my_auto_load'], true, true);
// spl_autoload_register('my_auto_load');
ArrayHelper::demo();
