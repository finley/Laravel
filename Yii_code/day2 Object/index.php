<?php

include('./Object.php');

class MyObject extends Object {
	private $_label;

	public function setLabel($value)
	{
		$this->_label = $value;
	}

	public function getLabel()
	{
		return $this->_label;
	}

}

$object = new MyObject(['Label' => 'finley']);

// 等价于  $object->setLabel('abc');
$object->Label = 'abc';

echo $object->Label;

// MyObject
// echo MyObject::className();

//$object2 = new MyObject(['name' => 'finley', 'age' => 18]);
//echo $object2->age;