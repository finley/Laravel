<?php
/**
 * Created by PhpStorm.
 * User: MF
 * Date: 2018/6/24
 * Time: 22:14
 */

class BaseYii
{
	public static function configure($object, $properties)
	{
		foreach ($properties as $name => $value) {
			$object->$name = $value;
		}

		return $object;
	}

}

class Yii extends BaseYii
{
}
