<?php

include 'BaseYii.php';

interface Configurable 
{

}

class Object implements Configurable
{
	public static function className()
	{
		return get_called_class();
	}

	public function __construct($config = [])
	{
		if (!empty($config)) {
			Yii::configure($this, $config);
		}

		$this->init();
	}

	public function init()
	{
	}

	public function __get($name) 
	{
		$getter = 'get' . $name;
		if (method_exists($this, $getter)) {
			return $this->$getter();
		} else {
			throw new Exception('Getting unknow property: ' . get_class($this) . '::' . $name);
		}
	}

	public function __set($name, $value)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        } else {
            throw new Exception('Setting unknown property: ' . get_class($this) . '::' . $name);
        }
    }


    // 当使用 isset 或 empty 时调用
    public function __isset($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter() !== null;
        } else {
            return false;
        }
    }

    //
	public function __call($name, $params)
	{
		throw new UnknownMethodException('Calling unknown method: ' . get_class($this) . "::$name()");
	}


	public function hasProperty($name, $checkVars = true)
    {
        return $this->canGetProperty($name, $checkVars) || $this->canSetProperty($name, false);
    }


	// 属性可读
    public function canGetProperty($name, $checkVars = true)
    {
        return method_exists($this, 'get' . $name) || $checkVars && property_exists($this, $name);
    }


    // 属性可写
    public function canSetProperty($name, $checkVars = true)
    {
        return method_exists($this, 'set' . $name) || $checkVars && property_exists($this, $name);
    }


    public function hasMethod($name)
    {
        return method_exists($this, $name);
    }

}
