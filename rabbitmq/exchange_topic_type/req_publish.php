<?php
/**
 * @发布消息
 * @Author: Helloweba
 * @publish_direct.php
 */

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../config.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$exchange = 'topic.exchange';

// connection -> channel -> exchange -> publish -> close
$config = getConfig();
$connection = new AMQPStreamConnection(
    $config['host'],
    $config['port'],
    $config['user'],
    $config['password']
);

$channel = $connection->channel();

$channel->exchange_declare($exchange, 'topic', false, false, false);

$channel->queue_declare("topic.queue.a", false, false, true, false);
$channel->queue_declare("topic.queue.b", false, false, true, false);
$channel->queue_declare("topic.queue.c", false, false, true, false);

$channel->queue_bind("topic.queue.a", $exchange, 'sh.#');
$channel->queue_bind("topic.queue.b", $exchange, 'sz.*.deal');
$channel->queue_bind("topic.queue.c", $exchange, '*.stock.deal');

$routing_key = $_POST['route'];
$msg = $_POST['msg'];

$data = new AMQPMessage($msg);

$channel->basic_publish($data, $exchange, $routing_key);
// Send history message: {"id":"message_41","content":"type_history"}
echo 'Send '.$routing_key.' message: ' . $msg . PHP_EOL;

//$channel->close();
//$connection->close();
