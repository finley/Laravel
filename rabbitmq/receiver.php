<?php

/**
 * @Author: Helloweba
 * @receiver.php
 * @消息消费者-接收端
 */
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;

$queue = 'worker';
$config = getConfig();
$connection = new AMQPStreamConnection(
    $config['host'],
    $config['port'],
    $config['user'],
    $config['password']
);

$channel = $connection->channel();

$channel->queue_declare($queue, false, true, false, true);

echo ' [*] Waiting for messages. To exit press CTRL+C' . PHP_EOL;

$callback = function($msg){
    echo " Received message：", $msg->body, PHP_EOL;
    sleep(1);  //模拟耗时执行
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};

// 处理和确认完消息后再消费新的消息
$channel->basic_qos(null, 1, null);

// 第4个参数值为false表示启用消息确认
$channel->basic_consume($queue, '', false, false, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();
