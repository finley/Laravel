<?php


/**
 * 
 */
class Cart 
{

	private $prefix = 'cart:';

	private $redis = null;

	public function __construct()
	{
        $this->redis = new Redis();
        $this->redis->connect('127.0.0.1',6379);
    }

    public function userId()
    {
    	return 1;
    }

    public function addItem($goodId, $count)
    {
    	$key = $this->prefix . $this->userId();

    	$oldCount = $this->getItem($goodId);

    	if ($oldCount) {
    		$newCount = $oldCount + $count;
			$this->redis->hset($key, $goodId, $newCount);

    	} else {
    		$this->redis->hset($key, $goodId, $count);
    	}
    }

	public function getItem($goodId)
	{
		$key = $this->prefix . $this->userId();
		return $this->redis->hget($key, $goodId);
	}
}

$cart = new Cart();



$cart->addItem('106', 3);

$cart->addItem('106', -2);
