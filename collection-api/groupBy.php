<?php

require __DIR__ . '/vendor/autoload.php';

/**
 *  Github有个api https://api.github.com/users/mafeifan/events
 *  记录最近一段时间用户的操作，如star，push，watch，Issues等不同的event
 *  每个event按照不同的分数，汇总总得分，计算用户的活跃度
 */


$collection = collect([
    ['account_id' => 'account-x10', 'product' => 'Chair'],
    ['account_id' => 'account-x10', 'product' => 'Bookcase'],
    ['account_id' => 'account-x11', 'product' => 'Desk'],
]);

// 二维数组无效
$grouped = $collection->forget('account_id');


dd($grouped->all());

