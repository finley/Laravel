<?php

require __DIR__ . '/vendor/autoload.php';


$lastYear = [
	100,
	110,
	220
];

$thisYear = [
	120,
	150,
	200
];

// zip 有拉链的意思
// zip合并数组 => [ [100, 120], [110, 150], [220, 200] ]
// 每个item都是collection类型
$result = collect($thisYear)->zip($lastYear)->map(
	function ($item) {
		return $item[0] - $item[1];
	}
);

dd($result);

exit();
