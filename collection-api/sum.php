<?php

require __DIR__ . '/vendor/autoload.php';

// 按照日期分组，然后计算总价(单价*数量)

$orders = [
	[
		'id'    => 1,
		'price' => 9.8,
		'qty'   => 2,
		'date'  => '2018-10-10'
	],
	[
		'id'    => 2,
		'price' => 3.8,
		'qty'   => 1,
		'date'  => '2018-10-10'
	],
	[
		'id'    => 3,
		'price' => 5.0,
		'qty'   => 2,
		'date'  => '2018-10-11'
	]
];

// 先按date分组
// 对每组进行汇总
$result = collect($orders)->groupBy('date')->map(
	function ($item) {
		return $item->sum(
			function ($item) {
				return $item['price'] * $item['qty'];
			}
		);
	}
);

dd($result->all());
