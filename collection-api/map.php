<?php

require __DIR__ . '/vendor/autoload.php';


$items = [
    ['name' => 'Finley Ma', 'age' => 18],
    ['name' => 'Jack Zhang', 'age' => 28],
];

$result = collect($items)
    ->map(function ($item, $key) {
       $item['firstName'] = explode(' ',$item['name'])[1];
       return $item;
    })
    ->pluck('firstName', 'age')
    ->all();

dd($result);

exit();
