<?php

require __DIR__ . '/vendor/autoload.php';

$matchA = [
    'flights' => [
        // parties
        [
            // players
            [
                0 => [
                    'relName' => 'Luder'
                ]
            ],
            [
                0 => [
                    'relName' => 'Luder'
                ]
            ]
        ],
        [
            // players
            [
                0 => [
                    'relName' => 'Luder'
                ]
            ],
            [
                0 => [
                    'relName' => 'Luder'
                ]
            ]
        ]
    ]
];

$players = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

$matchA = [
    ['a', 'b'], ['c', 'd'], ['e', 'f'], ['g', 'h']
];

$matchB = [
    ['b', 'c'], ['a', 'd'], ['e', 'g'], ['f', 'h']
];

// 求 matchC

$combined = [['a', 'b'], ['c', 'd'], ['e', 'f'], ['g', 'h'], ['b', 'c'], ['a', 'd'], ['e', 'g'], ['f', 'h']];

$result = [];

for ($i = 0; $i < count($players); $i++) {
    $left = $players[$i];
    $result[$left] = [];
    for ($j = 0; $j < count($combined); $j++) {
        $combinedItem = $combined[$j];
        if (in_array($left, $combinedItem)) {
            array_push($result[$left], ...$combinedItem);
        }
    }
}

// 处理
/**
 *  [
 *    'a' => ['b', 'd'],
 *    'b' => ['a', 'c']
 *  ]
 */
foreach ($result as $key => $item) {
    $result[$key] = array_diff($item, [$key]);
}

$matchC = [];


var_export($result);

