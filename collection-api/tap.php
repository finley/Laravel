<?php

require __DIR__ . '/vendor/autoload.php';

// tap 不会修改原集合，主要为来测试

$items = [
    ['name' => 'David Charleston', 'member' => 1, 'active' => 1],
    ['name' => 'Blain Charleston', 'member' => 0, 'active' => 0],
    ['name' => 'Megan Tarash', 'member' => 1, 'active' => 1],
    ['name' => 'Jonathan Phaedrus', 'member' => 1, 'active' => 1],
    ['name' => 'Paul Jackson', 'member' => 0, 'active' => 1]
];

return collect($items)
    ->where('active', 1)
    ->tap(function($collection){
        return var_dump($collection->pluck('name'));
    })
    ->where('member', 1)
    ->tap(function($collection){
        return var_dump($collection->pluck('name'));
    });

// pipe 会修改原集合
return collect($items)
    ->where('active', 1)
    ->pipe(function ($collection) {
        return $collection->push(['name' => 'John Doe']);
    });
