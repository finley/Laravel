<?php

require __DIR__ . '/vendor/autoload.php';


$gates = [
	'aa_11',
	'bb_22',
	'cc_c2_33',
];

// 返回结果 ["11", "22", "33"]
$result = collect($gates)->map(function($gate) {
	return collect(explode('_', $gate))->last();
});

var_dump($result);

exit();
