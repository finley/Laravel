<?php

require __DIR__ . '/vendor/autoload.php';


$messages = [
	'I am ok',
	'you looks nice',
	'bla bla'
];

// 每个item都是collection类型
$result = collect($messages)->map(
	function ($item) {
		return '- ' . $item . '\n';
	}
)->implode('');

dd($result);

