<?php

require __DIR__ . '/vendor/autoload.php';

$months = [
	['month' => 1, 'total' => 100],
	['month' => 2, 'total' => 200],
];

$result = [];
for ($i = 1; $i <= 12; $i ++) {
	$findItem = collect($months)->filter(function($value, $key) use ($i) {
		return $value['month'] == $i;
	})->first();

	if ($findItem) {
		array_push($result, $findItem);
	} else {
		array_push($result, ['month' => $i, 'total' => 0]);
	}
}


// 返回结果 ["11", "22", "33"]
//
//


var_dump($result);

exit();
