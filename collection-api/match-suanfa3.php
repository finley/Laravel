<?php

require __DIR__ . '/vendor/autoload.php';

$matchA = [
    ['a', 'b'], ['c', 'd'], ['e', 'f'], ['g', 'h']
];

$matchB = [
    ['b', 'c'], ['a', 'd'], ['e', 'g'], ['f', 'h']
];

// 求 matchC

$combined = [['a', 'b'], ['c', 'd'], ['e', 'f'], ['g', 'h'], ['b', 'c'], ['a', 'd'], ['e', 'g'], ['f', 'h']];

$players = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

$result = [];

for ($i = 0; $i < count($players) / 2; $i++) {
    $item = [
        $players[$i], $players[count($players)-$i-1]
    ];
    if (count($item) === 2) {
        $left = $item[0];
        $right = $item[1];

        if (!in_array($item, $combined) || !in_array([$right, $left], $combined)) {
            array_push($result, $item);
        }
    }
//    $left = $players[$i];
//    $result[$left] = [];
//    for ($j = 0; $j < count($combined); $j++) {
//        $combinedItem = $combined[$j];
//        if (in_array($left, $combinedItem)) {
//            array_push($result[$left], ...$combinedItem);
//        }
//    }
}

var_export($result);

// 处理
/**
 *  [
 *    'a' => ['b', 'd'],
 *    'b' => ['a', 'c']
 *  ]
 */
//foreach ($result as $key => $item) {
//    $result[$key] = array_diff($item, [$key]);
//}
//
//$matchC = [];
//
//
//var_export($result);

