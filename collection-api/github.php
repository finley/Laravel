<?php

require __DIR__ . '/vendor/autoload.php';

/**
 *  Github有个api https://api.github.com/users/mafeifan/events
 *  记录最近一段时间用户的操作，如star，push，watch，Issues等不同的event
 *  每个event按照不同的分数，汇总总得分，计算用户的活跃度
 */


$events = json_decode(file_get_contents('./json/github-events.json'));

$result = collect($events)->pluck('type')->map(function ($eventType){
	return collect([
		'PushEvent'         => 5,
		'CreateEvent'       => 4,
		'IssueEvent'        => 3,
		'IssueCommentEvent' => 2
	])->get($eventType, 1);
})->sum();


dd($result);

