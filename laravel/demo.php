<?php

trait Macroable
{
    protected static $macros = [];

    public static function macro($name, $macro)
    {
        static::$macros[$name] = $macro;
    }

    public static function hasMacro($name)
    {
        return isset(static::$macros[$name]);
    }

    public static function mixin($minxin, $replace = true)
    {
        $methods = (new ReflectionClass($minxin))->getMethods(ReflectionMethod::IS_PUBLIC | ReflectionMethod::IS_PROTECTED);

        foreach ($methods as $method) {
            if ($replace || !static::hasMacro($method)) {
                $method->setAccessible(true);
                // https://www.php.net/manual/zh/reflectionmethod.invoke.php
                // 执行一个反射的方法
                static::macro($method->name, $method->invoke($minxin));
            }
        }
    }

    public static function __callStatic($method, $parameters)
    {
        if (! static::hasMacro($method)) {
            throw new BadMethodCallException(sprintf(
                'Method %s::%s does not exist.', static::class, $method
            ));
        }

        $macro = static::$macros[$method];

        if ($macro instanceof Closure) {
            return call_user_func_array(Closure::bind($macro, null, static::class), $parameters);
        }

        return $macro(...$parameters);
    }

    public function __call($method, $parameters)
    {
        if (!static::hasMacro($method)) {
            throw new BadMethodCallException(sprintf(
                'Method %s::%s does not exist.', static::class, $method
            ));
        }

        $macro = static::$macros[$method];

        if ($macro instanceof Closure) {
            return call_user_func_array($macro->bindTo($this, static::class), $parameters);
        }

        return $macro(...$parameters);
    }
}


class Father
{
    public function say()
    {
        // 返回的是闭包
        return function () {
            echo 'say';
        };
    }

    protected function eat()
    {
        return function () {
            echo 'eat';
        };
    }

//    public function test()
//    {
//        // 这样没有意义，要返回闭包
//        echo 'test';
//    }

}

class Child
{
    use Macroable;
}

Child::mixin(new Father());

$child = new Child();
$child->eat();
// 因为加了 __callStatic 支持静态调用
$child::eat();

//$methods = (new ReflectionClass($f))->getMethods(
//    ReflectionMethod::IS_PUBLIC | ReflectionMethod::IS_PROTECTED
//);
//
//var_dump($methods);
//
//foreach ($methods as $method) {
//    $method->setAccessible(true);
//}