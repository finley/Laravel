<?php


    function generate_validation_Code()
    {
        return mt_rand(10,100);
    }


    function generateUniqueValidationCodes(int $number)
    {
        $codeArr = [];
        for ($i = 0; $i < $number; $i++) {
            $codeArr[$i] = generate_validation_Code();
        }

        // 去重
        if (count(array_unique($codeArr)) != count($codeArr))  {
        	// echo '重复啦';
        	
					return generateUniqueValidationCodes($number);
        }

        $result = [45, 27, 78, 63, 83];

        // $result = ValidationCodes::query()->whereIn('validation_code', $codeArr)->get();




        if (!$result->isEmpty()) {

        		// 计算出重复的个数
            $this->generateUniqueValidationCodes($number);
        } else {
            return $codeArr;
        }
    }
    // 
    

    print_r(generateUniqueValidationCodes(10));

?>