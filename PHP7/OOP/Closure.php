<?php

// 跟JS一样，定义匿名函数
$say = function(){
	return '我是匿名函数'. "\n";
};


echo $say();

// 闭包可以当做参数
function test(Closure $callback){
	return $callback();
}


echo test($say);


class A {
    private $foo = 2;
}

$cl2 = function() {
    return $this->foo;
};



// 匿名函数绑定到一个新的对象
// 该函数返回一个全新的匿名的函数
// 因为访问的属性为私有属性，所以要指定第三个参数改变作用域，使其变为公有属性
// $bcl2 = Closure::bind($cl2, new A(), 'A');

// echo $bcl2();



class Person {
    private $name = 'finley';
    private static $age = '18';
}


$p = new Person();

//$p->name;
// $p::$age;


$getName = function() {
	return $this->name;
};


$getAge = function() {
	return Person::$age;
};

$t1 = Closure::bind($getName, new Person(), 'Person');

echo $t1();


$t2 = Closure::bind($getAge, null, 'Person');

echo $t2();