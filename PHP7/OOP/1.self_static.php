<?php

/**
 *  1.在 PHP 里，在没有继承时候，你用self::class 和 static::class是一样的，都是获取当前类名。
 *  2.如果用到了继承,self 指向的是当前方法存在的这个类，也就是父类。static 指向的是最终那个子类。
 */
class Father {
    public static function who() {
        return __CLASS__;
    }

    public function getNewFather() {
        // 始终返回基类，最终的类
        return new self();
    }

    // 返回调用他的类
    public function getNewCaller() {
        return new static();
    }

    public static function test()
    {
        echo 'self::who() => ' . self::who();
        echo '/r/n';
        echo 'self::who() => ' . static::who();
    }
}

class Sun1 extends Father {
    public static function who() {
        return __CLASS__;
    }


}

class Sun2 extends Father {
}

$sun1 = new Sun1();
$sun2 = new Sun2();

// Father
print get_class($sun1->getNewFather());
// Sun1
print get_class($sun1->getNewCaller());
// Father
print get_class($sun2->getNewFather());
// Sun2
print get_class($sun2->getNewCaller());

echo Sun1::test();