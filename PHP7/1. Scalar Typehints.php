<?php

interface SomeInterFace {
	public function getUser() : User;
}

class User {}

class SomeClass implements SomeInterFace {
	public function getUser() : User {
		return [];
	}
}

(new SomeClass)->getUser();