<?php ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title></title>
</head>
<body>
  <button onclick="scanQRCode()">Click Me</button>
  <span id="content"></span>
</body>
<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.0.0/jquery.js"></script>
<script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script>

    function scanQRCode(){
        $("#content").text("");
        // 获取当前的url地址
        let url = location.href.split('#')[0] + '/';
        url = encodeURIComponent(url);
        // 发送请求获取 wx配置参数
        $.ajax({
            url: 'https://xiaochengxu.leeshuai.com/get_sign_package',
            data: {url: url},
            crossDomain: true,
            success: function(resp){
                // 数据返回成功
                if(resp){
                    // 初始化微信公众号信息
                    wx.config({
                        debug: true,
                        appId: resp.appId,
                        timestamp: resp.timestamp,
                        nonceStr: resp.nonceStr,
                        signature: resp.signature,
                        jsApiList: ['scanQRCode']
                    });
                    // 成功执行
                    wx.ready(() => {
                        wx.scanQRCode({
                            needResult: 1,
                            scanType: ["qrCode", "barCode"],
                            success: (res) => {
                                $("#content").text(JSON.stringify(res))
                                console.log(res.resultStr);// 扫码成功返回数据
                            },
                            fail: (err) => {
                                console.log(err) // 扫码失败
                            }
                        })
                    });
                    // 失败执行
                    wx.error((res) => {
                        console.log(res);
                    })

                }
            }, error: function(e){
                console.log(e);
            }
        })
    }

</script>
</html>
